import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';

import { createStackNavigator } from 'react-navigation'

import DrawerNavigator from './Screens/DrawerNavigator'

export default class App extends Component {
  render() {
    return (
      <AppStackNavigator />
    );
  }
}

const AppStackNavigator = new createStackNavigator(
  {
    DrawerNavigator: { screen: DrawerNavigator },
  },
  {
    navigationOptions:
    {
      gesturesEnabled:  false,
      header: null
    }
  }
)
