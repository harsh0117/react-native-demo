import React, { Component } from 'react'
import {
    StyleSheet,
    Text,
    View,
    Alert,
} from 'react-native';

import QRCodeScanner from 'react-native-qrcode-scanner';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Toast from 'react-native-simple-toast';

export default class QRScanner extends Component {

    onSuccess(e) {
        Toast.show(e.data);
    }

    render() {
        return (
            <View style = {{flex:1}}>
                <View style={styles.headerStyle}>
                    <Ionicons name="md-menu" size={30} style={{ flex: 1, color: 'white' }} onPress={() => this.props.navigation.openDrawer()} />
                    <Text style={styles.headerText}> Settings </Text>
                </View>
                <QRCodeScanner
                    onRead={this.onSuccess.bind(this)}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    centerText: {
        flex: 1,
        fontSize: 18,
        padding: 32,
        color: '#777',
    },
    headerStyle:
    {
        flexDirection: 'row',
        height: '10%',
        backgroundColor: "#3c86c5",
        paddingLeft: 10,
        paddingTop: 25,
        shadowColor: "black",
        elevation: 2,
        borderBottomWidth: 0.25,
        borderBottomColor: 'grey',
        elevation: 1
    },
    headerText:
    {
        flex: 7,
        fontWeight: '700',
        fontSize: 20,
        color: 'white',
        alignSelf: 'center',
        paddingLeft: 5,
        paddingBottom: 8
    },
}
)