import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Switch,
    Modal,
    TouchableOpacity,
    Button,
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons'

export default class AboutUs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            notification: true,
            modalOpen: false,
            selfState: 'all',
            tclState: 'all',
            mcuState: 'all',
            customModalOn: false,
        };
    }


    _renderOptions(temp) {
        switch (temp) {
            case 'selfState':
                return (
                    <View>
                        <View style={styles.options}>
                            <TouchableOpacity style={styles.optionButton} onPress={() => { this.setState({ selfState: 'all' }) }}>
                                <Ionicons name={this.state.selfState == 'all' ? 'md-radio-button-on' : 'md-radio-button-off'} />
                                <Text style={{ padding: 5 }}>All</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.optionButton} onPress={() => { this.setState({ selfState: 'none' }) }}>
                                <Ionicons name={this.state.selfState == 'none' ? 'md-radio-button-on' : 'md-radio-button-off'} />
                                <Text style={{ padding: 5 }}>None</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.optionButton} onPress={() => { this.setState({ selfState: 'custom' }) }}>
                                <Ionicons name={this.state.selfState == 'custom' ? 'md-radio-button-on' : 'md-radio-button-off'} />
                                <Text style={{ padding: 5 }}>Custom</Text>
                            </TouchableOpacity>
                        </View>
                        {this.state.selfState == 'custom' ? <View>
                            <TouchableOpacity onPress={() => { this.setState({ customModalOn: !this.state.customModalOn }) }}>
                                <Text>These are the extra options</Text>
                            </TouchableOpacity>
                        </View> : <View></View>}
                    </View>
                )
                break;
            case 'tclState':
                return (
                    <View>
                        <View style={styles.options}>
                            <TouchableOpacity style={styles.optionButton} onPress={() => { this.setState({ tclState: 'all' }) }}>
                                <Ionicons name={this.state.tclState == 'all' ? 'md-radio-button-on' : 'md-radio-button-off'} />
                                <Text style={{ padding: 5 }}>All</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.optionButton} onPress={() => { this.setState({ tclState: 'none' }) }}>
                                <Ionicons name={this.state.tclState == 'none' ? 'md-radio-button-on' : 'md-radio-button-off'} />
                                <Text style={{ padding: 5 }}>None</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.optionButton} onPress={() => { this.setState({ tclState: 'custom' }) }}>
                                <Ionicons name={this.state.tclState == 'custom' ? 'md-radio-button-on' : 'md-radio-button-off'} />
                                <Text style={{ padding: 5 }}>Custom</Text>
                            </TouchableOpacity>
                        </View>
                        {this.state.tclState == 'custom' ? <View>
                            <TouchableOpacity onPress={() => { this.setState({ customModalOn: !this.state.customModalOn }) }}>
                                <Text>These are the extra options</Text>
                            </TouchableOpacity>
                        </View> : <View></View>}
                    </View>

                )
                break;
            case 'mcuState':
                return (
                    <View>
                        <View style={styles.options}>
                            <TouchableOpacity style={styles.optionButton} onPress={() => { this.setState({ mcuState: 'all' }) }}>
                                <Ionicons name={this.state.mcuState == 'all' ? 'md-radio-button-on' : 'md-radio-button-off'} />
                                <Text style={{ padding: 5 }}>All</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.optionButton} onPress={() => { this.setState({ mcuState: 'none' }) }}>
                                <Ionicons name={this.state.mcuState == 'none' ? 'md-radio-button-on' : 'md-radio-button-off'} />
                                <Text style={{ padding: 5 }}>None</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.optionButton} onPress={() => { this.setState({ mcuState: 'custom' }) }}>
                                <Ionicons name={this.state.mcuState == 'custom' ? 'md-radio-button-on' : 'md-radio-button-off'} />
                                <Text style={{ padding: 5 }}>Custom</Text>
                            </TouchableOpacity>
                        </View>
                        {this.state.mcuState == 'custom' ? <View>
                            <TouchableOpacity onPress={() => { this.setState({ customModalOn: !this.state.customModalOn }) }}>
                                <Text>These are the extra options</Text>
                            </TouchableOpacity>
                        </View> : <View></View>}
                    </View>
                )
                break;
        }
    }

    _renderSection() {
        <View style={styles.container2}>
            <TouchableOpacity onPress={() => { this.stateData.self.isOn = !this.stateData.self.isOn }}>
                <View style={styles.label}><Text>Self</Text></View>
            </TouchableOpacity>
            {this.stateData.self.isOn == true ? this._renderOptions(this.stateData.self) : <View></View>}
        </View>
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerStyle}>
                    <Ionicons name="md-menu" size={30} style={{ flex: 1, color: 'white' }} onPress={() => this.props.navigation.openDrawer()} />
                    <Text style={styles.headerText}> Settings </Text>
                </View>
                <View style={{ marginTop: 5 }}>
                    <View style={{ borderBottomWidth: 2.5, borderBottomColor: '#dcdcdc' }}>
                        <Text style={styles.subheading}>Account Details</Text>
                    </View>
                    <View style={styles.item}>
                        <Ionicons name="md-person" style={styles.icon} />
                        <Text style={styles.details}> cspmigrationuser </Text>
                    </View>
                    <View style={styles.item}>
                        <Ionicons name="md-mail" style={styles.icon} />
                        <Text style={styles.details}> cspmigrationuser@yopmail.com </Text>
                    </View>
                    <View style={styles.item}>
                        <Ionicons name="md-people" style={styles.icon} />
                        <Text style={styles.details}> CUSTOMER </Text>
                    </View>
                    <View style={{ borderBottomWidth: 2.5, borderBottomColor: '#dcdcdc', paddingTop: 5, borderTopWidth: 0.5, borderTopColor: 'grey' }}>
                        <Text style={styles.subheading}>Notification Settings</Text>
                    </View>
                    <View style={styles.item}>
                        <Ionicons name="md-notifications" style={styles.icon} />
                        <Text style={styles.details}> In-App Notifications </Text>
                        <Switch
                            onValueChange={(value) => this.setState({ notification: value })}
                            value={this.state.notification}
                            style={{ marginRight: 20 }}
                        />
                    </View>
                    <TouchableOpacity style={styles.item} onPress={() => this.setState({ modalOpen: true })}>
                        <Text style={{ flex: 8, paddingTop: 5, color: 'black', marginLeft: 10 }}>Mail Notification Settings</Text>
                        <Ionicons name="ios-arrow-forward" style={{ flex: 1, fontSize: 24 }} />
                    </TouchableOpacity>
                </View>
                <Modal
                    visible={this.state.modalOpen}
                    animationType="slide"
                    transparent={true}
                    shouldCloseOnOverlayClick={true}
                    onRequestClose={() => this.setState({ modalOpen: false })}
                >
                    <View style={styles.headerStyle}>
                        <Ionicons name="ios-arrow-back" size={30} style={{ flex: 1, color: 'white' }} onPress={() => this.setState({ modalOpen: false })} />
                        <Text style={styles.headerText}> Mail Settings </Text>
                    </View>
                    <View style={styles.container2}>
                        <TouchableOpacity onPress={() => { this.setState({ self: !this.state.self }) }}>
                            <View style={styles.label}><Text>Self</Text></View>
                        </TouchableOpacity>
                        {this.state.self == true ? this._renderOptions('selfState') : <View></View>}
                        <TouchableOpacity onPress={() => { this.setState({ tcu: !this.state.tcu }) }}>
                            <View style={styles.label}><Text>Tata Communications Ltd</Text></View>
                        </TouchableOpacity>
                        {this.state.tcu == true ? this._renderOptions('tclState') : <View></View>}
                        <TouchableOpacity onPress={() => { this.setState({ mcu: !this.state.mcu }) }}>
                            <View style={styles.label}><Text>My Company Users</Text></View>
                        </TouchableOpacity>
                        {this.state.mcu == true ? this._renderOptions('mcuState') : <View></View>}
                    </View>
                    <Modal
                        visible={this.state.customModalOn}
                        animationType="fade"
                        transparent={true}
                        shouldCloseOnOverlayClick={true}
                        onRequestClose={() => this.setState({ modalOpen: false })}
                    >
                        <View style={styles.customModalContainer}>
                            <View style={styles.customModalLayout}>
                                <View style={{ flex:1, padding: 4, borderBottomWidth: 0.5, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ fontSize: 20, flex: 6, fontWeight: 'bold', textAlign: "center" }}>SERVICE REQUEST</Text>
                                    <Switch style={{ flex: 2 }} />
                                </View>
                                <View style={{flex:3}}>
                                    <Text>1st Line of text</Text>
                                    <Text>2nd Line of text</Text>
                                    <Text>3rd Line of text</Text>
                                </View>
                                <View style={{flex:1}}>
                                    <TouchableOpacity onPress={() => this.setState({ customModalOn: false })} style={{ alignSelf: "center" }}><Text>close</Text></TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Modal>
                </Modal>
            </View>
        );
    }
}


const styles = StyleSheet.create(
    {
        container:
        {
            flex: 1,
        },
        headerStyle:
        {
            flexDirection: 'row',
            height: '10%',
            backgroundColor: "#3c86c5",
            paddingLeft: 10,
            paddingTop: 25,
            shadowColor: "black",
            elevation: 2,
            borderBottomWidth: 0.25,
            borderBottomColor: 'grey',
            elevation: 1
        },
        headerText:
        {
            flex: 7,
            fontWeight: '700',
            fontSize: 20,
            color: 'white',
            alignSelf: 'center',
            paddingLeft: 5,
            paddingBottom: 8
        },
        subheading:
        {
            fontSize: 16.5,
            fontWeight: '800',
            color: '#a6a6a6',
            marginTop: 12,
            marginBottom: 12,
            marginLeft: 10
        },
        item:
        {
            flexDirection: 'row',
            paddingTop: 10,
            paddingBottom: 10,
            borderBottomWidth: 2,
            borderBottomColor: '#dcdcdc',
            alignItems: 'flex-start'
        },
        icon:
        {
            flex: 1,
            fontSize: 24,
            color: 'black',
            marginLeft: 10
        },
        details:
        {
            flex: 8,
            paddingTop: 5,
            color: 'black'
        },
        container2: {
            flex: 1,
            backgroundColor: '#e9e9ef',
        },
        welcome: {
            fontSize: 20,
            textAlign: 'center',
            margin: 10,
        },
        instructions: {
            textAlign: 'center',
            color: '#333333',
            marginBottom: 5,
        },
        label: {
            borderWidth: 1,
            margin: 5,
            padding: 20,
        },
        options: {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            padding: 10,
        },
        optionButton: {
            padding: 5,
            flexDirection: 'row',
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
        },
        customModalContainer: {
            height: "100%",
            width: "100%",
            backgroundColor: 'rgba(150,150,150,0.8)'
        },
        customModalLayout: {
            position: "absolute",
            height: "30%",
            width: "70%",
            left: "15%",
            top: "35%",
            backgroundColor: "rgb(255,255,255)"
        }
    }
)