import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
  AsyncStorage,
  TouchableOpacity, 
  Alert,
  Button, 
  RefreshControl,
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons'

export default class Notifications extends Component {  

    constructor(props)
    {
        super(props);
        //this.data = require("./notificationData");
        //AsyncStorage.setItem("notification",JSON.stringify(this.data));
        
        this.state=
        {
          val:null,
          refreshing: false,
          foo: "",
        }
        this.checkStorage()
    }

    checkStorage = async () =>
    {
      try{
        let notification = await AsyncStorage.getItem('notification')
        if(notification == null)
        {
          //Alert.alert("Its null boi!!!")
          data = require("./notificationData");
          this.setState({
            val:data
          })
          AsyncStorage.setItem("notification",JSON.stringify(data));
        }
        else
        {
          data = JSON.parse(notification)
          this.setState({val: data})
        }
      }
      catch(error){
        Alert.alert(error)
      }
    }

    _onRefresh()
    {
      this.setState(
        {
          refreshing: true
        }
      )
      setTimeout(function() {
        this.setState({
          refreshing: false
        })
      }.bind(this),1000)
    }
  
    _keyExtractor(item,index){
      return index;
    }

    change(id)
    {
        data = this.state.val;
        data[id].seen = true;
        this.setState({val:data});
        AsyncStorage.setItem('notification', JSON.stringify(data))
    }

    renderItem(data){
        let {item,index} = data;
        return (
          <TouchableOpacity 
            onPress = {()=> this.change(item.id)}
          >
            <View style={[styles.listitem,{backgroundColor:item.seen==true?'white':'lightblue'}]}>
                <View style={styles.box}>
                  <Ionicons name = "md-alert" style = {{fontSize: 30, color: 'orange', paddingLeft: 10}} />
                </View>
                <View style={{flexDirection:'column',flex:8}}>
                  <Text style={{paddingBottom:5,fontWeight:'bold',fontSize:18}}>{item.title}</Text>
                  <Text style={{padding:5}}>{item.message}</Text>
                  <Text style={{paddingTop:5}}>{item.time}</Text>
                </View>
            </View>
          </TouchableOpacity>
        ); 
      }


      displayData = async () => {
        try{
          let notification = await AsyncStorage.getItem('notification')
          this.parsed = JSON.parse(notification)
          Alert.alert(this.parsed[1].title)
        }
        catch(error){
          Alert.alert(error)
        }
      }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerStyle}>
                    <Ionicons name="md-menu" size = {30} style = {{flex:1, color: 'white'}} onPress = {() => this.props.screenProps.navigation.openDrawer()}/>
                    <Text style = {styles.headerText}> Notifications </Text>
                </View>
                <View>
                  <FlatList
                      keyExtractor={this._keyExtractor}
                      data = {this.state.val}
                      renderItem={this.renderItem.bind(this)}
                      refreshControl = {
                          <RefreshControl
                              refreshing = {this.state.refreshing}
                              onRefresh = {this._onRefresh.bind(this)}
                          />
                      }
                  />
                </View>
            </View>
    );
  }
}

const styles = StyleSheet.create(
    {
        container:
        {
            flex:1,
        },
        headerStyle:
        {
            flexDirection: 'row',
            height:'10%',
            backgroundColor:"#3c86c5",
            paddingLeft: 10,
            paddingTop:25,
            shadowColor:"black",
            elevation:2,
            borderBottomWidth: 0.25,
            borderBottomColor: 'grey',
            elevation: 1
        },
        headerText:
        {
            flex: 7,
            fontWeight: '700',
            fontSize: 20,
            color: 'white',
            alignSelf: 'center',
            paddingLeft: 5,
            paddingBottom: 8
        },
        listitem:{
            flexDirection:'row',
            height: 130,
            margin:1,
            paddingTop:15,
            paddingBottom:15,
          },
          box:{
            flex:1,
            justifyContent:'center',
            padding:0
          }
    }
)