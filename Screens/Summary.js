import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    FlatList,
    Modal,
    TextInput,
    TouchableOpacity,
    Alert,
    RefreshControl,
    ScrollView,
    Button,
    Linking,
    Platform,
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import ModalDropdown from 'react-native-modal-dropdown';

export default class Summary extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentCity: "something",
            modalOpen: false,
            listModalOpen: false,
            detailModalOpen: false,
            data: [{ "id": 1, "Country": "India", 'city1': { "cityname": 'Chennai', 'mintemp': 32, 'maxtemp': 40 }, 'city2': { "cityname": 'Mumbai', 'mintemp': 27, 'maxtemp': 36 }, 'city3': { "cityname": 'Delhi', 'mintemp': 23, 'maxtemp': 38 } },
            { "id": 1, "Country": "USA", 'city1': { "cityname": 'San Jose', 'mintemp': 14, 'maxtemp': 22 }, 'city2': { "cityname": 'Stockton', 'mintemp': 12, 'maxtemp': 18 }, 'city3': { "cityname": 'New York City', 'mintemp': 8, 'maxtemp': 12 } },
            { "id": 1, "Country": "UK", 'city1': { "cityname": 'Birmingham', 'mintemp': 18, 'maxtemp': 20 }, 'city2': { "cityname": 'Leeds', 'mintemp': 20, 'maxtemp': 24 }, 'city3': { "cityname": 'Manchester', 'mintemp': 15, 'maxtemp': 18 } },
            { "id": 1, "Country": "Australia", 'city1': { "cityname": 'Sydney', 'mintemp': 10, 'maxtemp': 16 }, 'city2': { "cityname": 'Armidale', 'mintemp': 12, 'maxtemp': 15 }, 'city3': { "cityname": 'Newcastle', 'mintemp': 16, 'maxtemp': 22 } }],
            menuOn: '100%',
            issuetype: "",
            impact: "",
            description: "",
            dropOption: 'Select',
            optionsData: ['Chennai', 'Bangalore', 'Jharkand', 'Mumbai', 'Delhi'],
        }
    }

    renderItem(data) {
        let { item, index } = data;
        return (
            <View style={styles.listItem}>
                <View style={[styles.countryTitle, { padding: 15 }]}>
                    <Text style={styles.countryTitleText}>{item.Country}</Text>
                </View>
                <View style={styles.citydata}>
                    <View style={{ flexDirection: 'row', padding: 10 }}>
                        <Text style={{ flex: 2 }}></Text>
                        <Text style={{ flex: 1, textAlign: "center" }}>MIN</Text>
                        <Text style={{ flex: 1, textAlign: "center" }}>MAX</Text>
                    </View>
                    <View style={{ flexDirection: 'row', padding: 10, borderBottomWidth: 0.25, borderBottomColor: 'lightgrey' }}>
                        <Text style={{ flex: 2 }}>{item.city1.cityname}</Text>
                        <TouchableOpacity style={{ flex: 1 }} onPress={() => this.setState({ listModalOpen: true, currentCity: item.city1.cityname })}>
                            <Text style={styles.data}>{item.city1.mintemp}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ flex: 1 }} onPress={() => this.setState({ listModalOpen: true, currentCity: item.city1.cityname })}>
                            <Text style={styles.data}>{item.city1.maxtemp}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: 'row', padding: 10, borderBottomWidth: 0.25, borderBottomColor: 'lightgrey' }}>
                        <Text style={{ flex: 2 }}>{item.city2.cityname}</Text>
                        <TouchableOpacity style={{ flex: 1 }} onPress={() => this.setState({ listModalOpen: true, currentCity: item.city2.cityname })}>
                            <Text style={styles.data}>{item.city2.mintemp}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ flex: 1 }} onPress={() => this.setState({ listModalOpen: true, currentCity: item.city2.cityname })}>
                            <Text style={styles.data}>{item.city2.maxtemp}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: 'row', padding: 10 }}>
                        <Text style={{ flex: 2 }}>{item.city3.cityname}</Text>
                        <TouchableOpacity style={{ flex: 1 }} onPress={() => this.setState({ listModalOpen: true, currentCity: item.city3.cityname })}>
                            <Text style={styles.data}>{item.city3.mintemp}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ flex: 1 }} onPress={() => this.setState({ listModalOpen: true, currentCity: item.city3.cityname })}>
                            <Text style={styles.data}>{item.city3.maxtemp}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }

    _keyExtractor(item, index) {
        return index;
    }

    //Code after this is written for the issue listing page

    _keyExtractor2(item, index) {
        return index.toString();
    }

    onFilterPress({ val = 'in' }) {
        switch (val) {
            case 'us': this.setState({ headerText: "United States of America" }); break;
            case 'gb': this.setState({ headerText: "United Kingdom" }); break;
            case 'in': this.setState({ headerText: "India" }); break;
            case 'jp': this.setState({ headerText: "Japan" }); break;
        }
        return fetch('https://newsapi.org/v2/top-headlines?country=' + val + '&apiKey=145cae52067945059c40a596650cb455')
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    isLoading: false,
                    dataSource: responseJson.articles,
                },
                    function () {
                    });
            })
            .catch((error) => {
                //console.error(error);
            });
    }

    renderItem2(dataSource) {
        let { item, index } = dataSource;
        return (
            <View>
                <TouchableOpacity
                    //onPress={() => this.props.navigation.navigate('SecondScreen', { issueNo: item.issueNo, message: item.text, status: item.status, timestamp: item.timestamp })}
                    onPress={() => this.setState({ detailModalOpen: true, detailTitle: item.title })}
                    style={styles.itemBlock}
                >
                    <View>
                        <View style={styles.itemBlockPart1}>
                            <Text style={styles.issueNo}>{item.title}</Text>
                        </View>
                        <Text style={styles.msg}>{item.description}</Text>
                        <View style={styles.status}>
                            <Text style={styles.statusText}>{item.source.name}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    renderSeparator() {
        return <View style={styles.separator} />
    }

    renderHeader(value) {
        return (
            <TouchableOpacity style={{ flexDirection: 'row', backgroundColor: '#e9e9e9', padding: 20 }} onPress={() => this.setState({ menuOn: '40%' })}>
                <MaterialIcons name="filter-list" style={{ fontSize: 24 }} />
                <Text style={{ fontSize: 20, paddingLeft: 10 }}> Filter </Text>
            </TouchableOpacity>
        )
    }

    _onRefresh() {
        this.setState({
            refreshing: true
        })
        setTimeout(function () {
            this.setState({
                refreshing: false
            })
        }.bind(this))
    }

    componentDidMount() {
        this.onFilterPress('Something')
    }

    _requestData() {
        fetch(Platform.OS === 'ios' ? 'http://localhost:8080/addtemp':'http://10.0.2.2:8080/addtemp', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "impact": this.state.issuetype,
                "cityName": this.state.dropOption,
                "comment": this.state.description,
            }),
        })
            .then((response) => response.json())
            .then((responseJson) => {
                Alert.alert(responseJson.impact + "\n" + responseJson.city_name + "\n" + responseJson.comment);
            })
            .catch((error) => {
                Alert.alert("Error");
            });
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerStyle}>
                    <Ionicons name="md-menu" size={30} style={{ flex: 1, color: 'white' }} onPress={() => this.props.screenProps.navigation.openDrawer()} />
                    <Text style={styles.headerText}> Weather </Text>
                    <Ionicons name="md-search" size={28} style={{ flex: 1, color: 'white' }} />
                    <Ionicons name="md-refresh" size={28} style={{ flex: 1, marginLeft: 5, color: 'white' }} />
                </View>
                <FlatList
                    keyExtractor={this._keyExtractor}
                    data={this.state.data}
                    renderItem={this.renderItem.bind(this)}
                />

                <View style={styles.hover}>
                    <TouchableOpacity onPress={() => this.setState({ modalOpen: true })}>
                        <Ionicons name="md-add-circle" style={{ fontSize: 60, color: "#3c86c5", elevation: 1 }} />
                    </TouchableOpacity>
                </View>
                <Modal
                    visible={this.state.modalOpen}
                    animationType="slide"
                    transparent={true}
                    shouldCloseOnOverlayClick={true}
                    onRequestClose={() => this.setState({ modalOpen: false })}
                >
                    <View style={styles.headerStyle}>
                        <Ionicons name="ios-arrow-back" size={30} style={{ flex: 1, color: 'white' }} onPress={() => this.setState({ modalOpen: false })} />
                        <Text style={styles.headerText}> Create </Text>
                    </View>
                    <View style={styles.create}>
                        <Text style={styles.createIncidentHeading}>Issue Type *</Text>
                        <TextInput style={styles.createIncidentInput} placeholder="Issue Type" onChangeText={(value) => this.setState({ issuetype: value })} />
                        <Text style={styles.createIncidentHeading}>Impact *</Text>
                        <ModalDropdown
                            style={styles.createIncidentInput}
                            options={this.state.optionsData}
                            onSelect={(index) => this.setState({ dropOption: this.state.optionsData[index] })}
                            dropdownStyle = {{width: '80%'}}
                        >
                            <View style={{flexDirection: 'row', width: '100%', justifyContent: 'space-between', paddingTop: 10, paddingRight: 10}}>
                                <Text>{this.state.dropOption}</Text>
                                <Ionicons name = "ios-arrow-down" size = {24} />
                            </View>
                        </ModalDropdown>
                        <Text style={styles.createIncidentHeading}>Short description *</Text>
                        <TextInput style={styles.createIncidentInput} placeholder="Description" onChangeText={(value) => this.setState({ description: value })} />
                        <TouchableOpacity onPress={() => this._requestData()}><Text style={styles.createIncidentSubmitButton}>Send</Text></TouchableOpacity>
                    </View>
                </Modal>
                <Modal
                    visible={this.state.listModalOpen}
                    animationType="slide"
                    shouldCloseOnOverlayClick={true}
                    onRequestClose={() => this.setState({ modalOpen: false })}
                >
                    <View>
                        <View style={styles.headerStyle}>
                            <Ionicons name="ios-arrow-back" size={30} style={{ flex: 1, color: 'white' }} onPress={() => this.setState({ listModalOpen: false })} />
                            <Text style={styles.headerText}> {this.state.currentCity} </Text>
                            <Ionicons name="md-search" size={30} style={{ flex: 1, color: 'white' }} />
                            <Ionicons name="md-refresh" size={30} style={{ flex: 1, paddingLeft: 20, color: 'white' }} />
                        </View>
                        <FlatList
                            keyExtractor={this._keyExtractor2}
                            data={this.state.dataSource}
                            renderItem={this.renderItem2.bind(this)}
                            ItemSeparatorComponent={this.renderSeparator.bind(this)}
                            ListHeaderComponent={this.renderHeader.bind(this)}
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this._onRefresh.bind(this)}
                                />
                            }
                        />
                        <ScrollView style={[styles.menu, { left: this.state.menuOn }]}>
                            <View style={{ borderBottomWidth: 1, flexDirection: 'row', justifyContent: 'space-between', padding: 12.8 }}>
                                <Text style={{ fontSize: 25, fontWeight: '700', marginTop: 5 }}> Filters </Text>
                                <Button
                                    title='X'
                                    onPress={() => {
                                        this.setState({ menuOn: '100%' })
                                    }}
                                />
                            </View>
                            <View style={styles.filterSeparators}>
                                <TouchableOpacity onPress={() => this.onFilterPress({ val: 'in' })} style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={[styles.filterTypes, { flex: 8 }]}>India</Text>
                                    <Text style={[styles.filterTypes, { flex: 2 }]}>{this.state.headerText == 'India' ? '✔️' : ''}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.filterSeparators}>
                                <TouchableOpacity onPress={() => this.onFilterPress({ val: 'us' })} style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={[styles.filterTypes, { flex: 8 }]}>USA</Text>
                                    <Text style={[styles.filterTypes, { flex: 2 }]}>{this.state.headerText == 'United States of America' ? '✔️' : ''}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.filterSeparators}>
                                <TouchableOpacity onPress={() => this.onFilterPress({ val: 'gb' })} style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={[styles.filterTypes, { flex: 8 }]}>UK</Text>
                                    <Text style={[styles.filterTypes, { flex: 2 }]}>{this.state.headerText == 'United Kingdom' ? '✔️' : ''}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.filterSeparators}>
                                <TouchableOpacity onPress={() => this.onFilterPress({ val: 'jp' })} style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={[styles.filterTypes, { flex: 8 }]}>Japan</Text>
                                    <Text style={[styles.filterTypes, { flex: 2 }]}>{this.state.headerText == 'Japan' ? '✔️' : ''}</Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </View>
                    <Modal
                        visible={this.state.detailModalOpen}
                        //animationType="fade"
                        shouldCloseOnOverlayClick={true}
                        onRequestClose={() => this.setState({ modalOpen: false })}
                    >
                        <View style={styles.headerStyle}>
                            <Ionicons name="ios-arrow-back" size={30} style={{ flex: 1, color: 'white' }} onPress={() => this.setState({ detailModalOpen: false })} />
                            <Text style={styles.headerText}>Details</Text>
                        </View>
                        <ScrollView>
                            <View style={{ borderBottomWidth: 1 }}>
                                <Text style={styles.headerText3}> {this.state.detailTitle} </Text>
                            </View>
                            <View style={styles.box}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingBottom: 10 }}>
                                    <Text style={{ color: '#757575', fontWeight: '500' }}> Resolution SLA expires in</Text>
                                    <Text style={{ color: '#00a9f7', fontWeight: '400' }}>ℹ other SLAs</Text>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'baseline' }}>
                                    <Text style={styles.timer1}> 01</Text>
                                    <Text style={styles.timer2}> days</Text>
                                    <Text style={styles.timer1}> : 01</Text>
                                    <Text style={styles.timer2}> mnts</Text>
                                    <Text style={styles.timer1}> : 01</Text>
                                    <Text style={styles.timer2}> seconds</Text>
                                </View>
                            </View>
                            <View style={styles.box}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingBottom: 10 }}>
                                    <Text style={{ color: '#757575', fontWeight: '500' }}> Status </Text>
                                    <Text style={{ color: '#00a9f7', fontWeight: '400' }}>ℹ audit log</Text>
                                </View>
                                <Text style={{ justifyContent: 'flex-start', textAlign: 'center', fontSize: 20, fontWeight: '500', paddingBottom: 3 }}> STATUS </Text>
                            </View>
                            <View style={styles.separator2}>
                                <Text style={styles.separatorText}> Incident Details </Text>
                                <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                                    <Ionicons name="md-attach" size={30} style={{ paddingRight: 10 }} onPress={() => Alert.alert('You tapped the Attach button!')} />
                                    <Ionicons name="ios-text" size={30} style={{ paddingLeft: 20, paddingRight: 20 }} onPress={() => Alert.alert('You tapped the Message button!')} />
                                </View>
                            </View>
                            <View>
                                <Text style={[styles.subheading, { paddingLeft: 5, paddingTop: 10 }]}> Customer </Text>
                                <Text style={{ paddingLeft: 5, color: '#00a9f7', fontSize: 14 }}> Customer name shall not be trimmed or emphasized </Text>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingLeft: 5, paddingTop: 5 }}>
                                    <Text style={[styles.subheading, { flex: 2 }]}> Issue Type </Text>
                                    <Text style={[styles.subheading, { flex: 1 }]}> Priority </Text>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center', flexWrap: 'wrap' }}>
                                    <Text style={{ flex: 2, paddingLeft: 5, justifyContent: 'center', flexWrap: 'wrap', fontSize: 14 }}> Issue type shall not be trimmed or _emphasized</Text>
                                    <Text style={{ flex: 1, paddingTop: 5, fontSize: 14 }}> P3</Text>
                                </View>
                                <Text style={[styles.subheading, { paddingTop: 10, paddingLeft: 5 }]}> Configuration Item (CI) </Text>
                                <Text style={{ paddingLeft: 5, color: '#00a9f7', fontSize: 14 }}> CHNINIT001 - 10.10.0.1 </Text>
                                <View style={{ flexDirection: 'row', paddingTop: 15, paddingLeft: 5 }}>
                                    <Text style={[styles.subheading, { flex: 1 }]}> Category </Text>
                                    <Text style={[styles.subheading, { flex: 1 }]}> Service </Text>
                                </View>
                                <View style={{ flexDirection: 'row', paddingLeft: 5 }}>
                                    <Text style={{ flex: 1, fontSize: 14 }}> Reactive</Text>
                                    <Text style={{ flex: 1, fontSize: 14 }}> Managed Hosting </Text>
                                </View>
                                <View style={{ flexDirection: 'row', paddingTop: 10, paddingLeft: 5 }}>
                                    <Text style={[styles.subheading, { flex: 1 }]}> Asignee Group </Text>
                                    <Text style={[styles.subheading, { flex: 1 }]}> Owner </Text>
                                </View>
                                <View style={{ flexDirection: 'row', paddingLeft: 5 }}>
                                    <Text style={{ flex: 1, fontSize: 14 }}> ABCD Network L2 </Text>
                                    <Text style={{ flex: 1, fontSize: 14 }}> Arunmuru </Text>
                                </View>
                            </View>
                            <View style={styles.separator2}>
                                <Text style={styles.separatorText}> Description </Text>
                            </View>
                            <Text style={{ margin: 5, fontSize: 14 }}> Hello World! </Text>
                            <View style={styles.separator2}>
                                <Text style={styles.separatorText}> Reporting Source Info </Text>
                            </View>
                            <View style={{ paddingLeft: 5 }}>
                                <View style={{ flexDirection: 'row', paddingTop: 10 }}>
                                    <Text style={[styles.subheading, { flex: 7 }]} > Name </Text>
                                    <Text style={[styles.subheading, { flex: 6 }]}> Reported On </Text>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ flex: 7 }} > Csp Migration User </Text>
                                    <Text style={{ flex: 6 }}> 2018-06-01 01:20:00 IST  </Text>
                                </View>
                                <Text style={[styles.subheading, { paddingTop: 10 }]}> Phone Number </Text>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Text style={[styles.innertext]}>+011 0234 2343 </Text>
                                    <Ionicons name="md-call" size={25} style={{ paddingLeft: 15 }} onPress={() => Linking.openURL('tel://+011 0234 2343')} />
                                </View>
                                <Text style={styles.subheading}> Email </Text>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Text> cspmigrationuser@yopmail.com </Text>
                                    <MaterialIcons name="email" size={25} style={{ paddingLeft: 15 }} onPress={() => Linking.openURL('mailto:cspmigrationuser@yopmail.com')} />
                                </View>
                                <Text style={styles.subheading}> CC Email </Text>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Text> cspmigrationuser@yopmail.com </Text>
                                    <MaterialIcons name="email" size={25} style={{ paddingLeft: 15 }} onPress={() => Linking.openURL('mailto:cspmigrationuser@yopmail.com')} />
                                </View>
                            </View>
                        </ScrollView>
                    </Modal>
                </Modal>
            </View>
        );
    }
}

const styles = StyleSheet.create(
    {
        container:
        {
            flex: 1,
        },
        listItem:
        {
            margin: 10,
            elevation: 1,
            backgroundColor: 'white',
            borderRadius: 4,
        },
        countryTitle:
        {
            backgroundColor: '#8dd4da',
        },
        countryTitleText:
        {
            fontSize: 18,
            fontWeight: 'bold',
            color: 'white',
        },
        citydata:
        {
            padding: 5,
        },
        data:
        {
            color: '#7da7cb',
            textAlign: "center"
        },
        headerStyle:
        {
            flexDirection: 'row',
            height: '10%',
            backgroundColor: "#3c86c5",
            paddingLeft: 10,
            paddingTop: 25,
            shadowColor: "black",
            elevation: 2,
            borderBottomWidth: 0.25,
            borderBottomColor: 'grey',
            elevation: 1
        },
        headerText:
        {
            flex: 7,
            fontWeight: '700',
            fontSize: 20,
            color: 'white',
            alignSelf: 'center',
            paddingLeft: 5,
            paddingBottom: 18
        },
        hover: {
            position: 'absolute',
            right: 20,
            top: '90%',
            borderRadius: 80
        },
        create: {
            padding: 5,
            flex: 1,
            //justifyContent: 'center',
            //alignItems: 'center',
            backgroundColor: '#F5FCFF',
        },
        itemBlock:
        {
            flexDirection: 'column',
            paddingTop: 15,
            paddingBottom: 15,
            paddingRight: 10,
            paddingLeft: 10,
            justifyContent: 'center'
        },
        itemBlockPart1:
        {
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingBottom: 10
        },
        issueNo:
        {
            fontSize: 15,
            fontWeight: '900',
        },
        timestamp:
        {
            color: '#6d6d6d',
            fontWeight: '200'
        },
        msg:
        {
            paddingBottom: 10
        },
        status:
        {
            overflow: 'hidden',
            borderRadius: 5,
            width: '30%'
        },
        statusText:
        {
            color: '#3678ab',
            backgroundColor: '#e0e0e0',
            padding: 5,
            textAlign: 'center',
        },
        separator:
        {
            height: 0.5,
            width: "100%",
            alignSelf: 'center',
            backgroundColor: "#555"
        },
        header:
        {
            flexDirection: 'row',
            justifyContent: 'space-between',
            padding: 10,
            borderBottomWidth: 1,
            alignItems: 'flex-end'
        },
        headerText2:
        {
            fontSize: 24,
            fontWeight: '400',
            width: '60%'
        },
        headerButton:
        {
            flexDirection: 'row',
            justifyContent: 'center'
        },
        menu:
        {
            width: '60%',
            maxHeight: '90%',
            position: 'absolute',
            backgroundColor: 'white',
            borderWidth: 0.6,
            borderColor: 'grey',
            top: 75,
        },
        filterTypes:
        {
            fontSize: 18,
            fontWeight: '300',
            marginBottom: 10,
            marginTop: 10,
            marginLeft: 15
        },
        filterSeparators:
        {
            borderBottomWidth: 0.5,
            borderBottomColor: 'grey'
        },
        filterSubHeadings:
        {
            fontSize: 14,
            color: '#6d6d6d',
            marginTop: 10,
            marginLeft: 5
        },
        headerText3:
        {
            fontSize: 20,
            fontWeight: '600',
            textAlign: 'center',
            paddingBottom: 10,
            paddingTop: 10
        },
        box:
        {
            borderWidth: 1.2,
            borderRadius: 5,
            overflow: 'hidden',
            margin: 10,
            marginBottom: 0,
            borderColor: '#ABABAB',
            padding: 5
        },
        timer1:
        {
            fontSize: 20,
            fontWeight: '600'
        },
        timer2:
        {
            fontSize: 12,
            fontWeight: '300',
            color: '#494949'
        },
        subheading:
        {
            fontSize: 12,
            fontWeight: '400',
            color: '#494949',
            justifyContent: 'flex-start',
        },
        separator2:
        {
            flexDirection: 'row',
            justifyContent: 'space-between',
            borderBottomWidth: 1,
            marginTop: 25,
            alignItems: 'baseline'
        },
        separatorText:
        {
            fontSize: 18,
            fontWeight: '800',
            color: '#595959',
            paddingBottom: 5
        },
        innertext:
        {
            paddingLeft: 5,
        },
        createIncidentHeading:
        {
            marginLeft: 20,
            paddingTop: 20,
            paddingBottom: 10,
            fontWeight: '900',
            color: '#595959'
        },
        createIncidentInput:
        {
            marginLeft: 20,
            marginRight: 20,
            borderWidth: 1,
            borderColor: 'lightgrey',
            height: 50,
            padding: 5,
            paddingLeft: 20
        },
        createIncidentSubmitButton:
        {
            fontSize: 24,
            borderWidth: 0.5,
            backgroundColor: '#135db6',
            alignSelf: 'center',
            marginTop: 25,
            paddingTop: 10,
            paddingBottom: 10,
            paddingLeft: 80,
            paddingRight: 80,
            color: 'white'
        }
    }
)