import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Linking
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons'

export default class Help extends Component {  
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerStyle}>
                        <Ionicons name="md-menu" size = {30} style = {{flex:1, color: 'white'}} onPress = {() => this.props.screenProps.navigation.openDrawer()}/>
                        <Text style = {styles.headerText}> Help & Support </Text>
                </View>
                <View style = {{marginLeft: 10}}>
                    <Text style ={{fontWeight:'900', fontSize: 16, marginTop: 22, marginBottom: 16}}> GSMC Helpdesk </Text>
                    <View style = {{flexDirection: 'row'}}>
                        <Text style = {styles.headings}> Email: </Text>
                        <TouchableOpacity onPress = {()=>Linking.openURL('mailto:gsmc.helpdesk@tatacommunications.com')}>
                            <Text style = {styles.hyperlink}> gsmc.helpdesk@tatacommunications.com</Text>
                        </TouchableOpacity>
                    </View>
                    <View style = {{flexDirection: 'row'}}>
                        <Text style = {styles.headings}> Global Direct: </Text> 
                        <TouchableOpacity onPress = {()=>Linking.openURL('tel://914461042800')}>
                            <Text style = {styles.hyperlink}> +91-44-61042800 </Text>
                        </TouchableOpacity>
                    </View>
                    <Text style = {{fontSize: 15, fontWeight: '700', marginTop: 15, marginBottom: 12}}> Toll Free </Text>
                    <View style = {{flexDirection: 'row'}}>
                        <Text style = {styles.headings}> India: </Text>
                        <TouchableOpacity onPress = {()=>Linking.openURL('tel://18002660665')}>
                            <Text style = {styles.hyperlink}> 1800 266 0665 </Text>
                        </TouchableOpacity>
                    </View>
                    <View style = {{flexDirection: 'row'}}>
                        <Text style = {styles.headings}> Singapore: </Text>
                        <TouchableOpacity onPress = {()=>Linking.openURL('tel://8001012567')}>
                            <Text style = {styles.hyperlink}> 800 101 2567 </Text>
                        </TouchableOpacity>
                    </View>
                    <View style = {{flexDirection: 'row'}}>
                        <Text style = {styles.headings}> United Kingdom(UK): </Text>
                        <TouchableOpacity onPress = {()=>Linking.openURL('tel://8000163014')}>
                            <Text style = {styles.hyperlink}> 800 016 3014 </Text>
                        </TouchableOpacity>
                    </View>
                    <View style = {{flexDirection: 'row'}}>
                        <Text style = {styles.headings}> United States(US): </Text>
                        <TouchableOpacity onPress = {()=>Linking.openURL('tel://18445190680')}>
                            <Text style = {styles.hyperlink}> 1 844 519 0680 </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
    );
  }
}

const styles = StyleSheet.create(
    {
        container:
        {
            flex:1,
        },
        headerStyle:
        {
            flexDirection: 'row',
            height:'10%',
            backgroundColor:"#3c86c5",
            paddingLeft: 10,
            paddingTop:25,
            shadowColor:"black",
            elevation:2,
            borderBottomWidth: 0.25,
            borderBottomColor: 'grey',
            elevation: 1
        },
        headerText:
        {
            flex: 7,
            fontWeight: '700',
            fontSize: 20,
            color: 'white',
            alignSelf: 'center',
            paddingLeft: 5,
            paddingBottom: 8
        },
        hyperlink:
        {
            color: '#0694cf',
            marginRight: '20%'
        },
        headings:
        {
            marginBottom: 10,
        }
    }
)