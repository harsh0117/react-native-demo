import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native';

export default class GetStarted extends Component {
  
    static navigationOptions = {
        header: null,
    }
    render() {
        return (
            <View style={styles.container}>
                <Button title = "Get Start" onPress = {()=> this.props.navigation.navigate('Summary')} />
            </View>
    );
  }
}

const styles = StyleSheet.create(
    {
        container:
        {
            flex:1,
            marginTop: 50,
            alignItems: 'center'
        }
    }
)

