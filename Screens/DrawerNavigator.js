import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';

import { createDrawerNavigator } from 'react-navigation'

import TabNavigator from './TabNavigator'
import AboutUs from './AboutUs'
import Settings from './Settings'
import QRCodeScanner from './QRCodeScanner'

export default class TheDrawerNavigator extends Component {
  render() {
    return (
      <AppDrawerNavigator />
    );
  }
}

const AppDrawerNavigator = new createDrawerNavigator({
  Summary: { screen: TabNavigator },
  AboutUs: { screen: AboutUs },
  Settings: { screen: Settings },
  QRCodeScanner: { screen: QRCodeScanner }
})
