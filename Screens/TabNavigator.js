import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native';

import { createBottomTabNavigator } from 'react-navigation'
import Ionicons from 'react-native-vector-icons/Ionicons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

import Summary from './Summary'
import Notification from './Notification'
import Help from './Help'

export default class AppTabNavigator extends Component
{
    render()
    {
        return(
                <HomeScreenTabNavigator screenProps = {{navigation: this.props.navigation}}/>
        )
    }
}

const HomeScreenTabNavigator = new createBottomTabNavigator(
    {
        Tickets: { 
            screen : Summary, 
            navigationOptions: {
                tabBarLabel: 'Tickets',
                tabBarIcon: () => (
                    <MaterialCommunityIcons name = "ticket-confirmation" size = {24}/>
                )
            }
        },
        Notifications: {
            screen: Notification,
            navigationOptions: {
                tabBarLabel: 'Notifications',
                tabBarIcon: () => (
                    <Ionicons name = "md-notifications" size = {24} />
                )
            }
        },
        Help: {
            screen: Help,
            navigationOptions: {
                tabBarLabel: 'Help',
                tabBarIcon: () => (
                    <Ionicons name = "md-help-circle" size = {24} />
                )
            }
        }
        
    }
)
