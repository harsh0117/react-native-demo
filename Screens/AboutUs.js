import React, { Component } from 'react';
import {
    Image,
    StyleSheet,
    Text,
    View,
    ScrollView,
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons'

export default class AboutUs extends Component {
    render() {
        let pic = {
            uri: 'https://stluciatimes.com/wp-content/uploads/2016/03/tata-communications_logo-560x390.jpg'
        };
        return (
            <View style={styles.container}>
                <View style={styles.headerStyle}>
                    <Ionicons name="md-menu" size={30} style={{ flex: 1, color: 'white' }} onPress={() => this.props.navigation.openDrawer()} />
                    <Text style={styles.headerText}> About Us </Text>
                </View>
                <ScrollView>
                    <Image source={pic} style={{ width: 265, height: 195, alignSelf: 'center'}} />
                    <Text style={styles.para}>
                        Over the past decade, Tata Communications has evolved from a wholesale service provider serving the Indian market to a leading provider of A New World of Communications™ to enterprise customers and service providers worldwide.
                    </Text>
                    <Text style={styles.para}>
                        Tata Communications is a global company with its roots in the emerging markets. Headquartered in Mumbai and Singapore, it has more than 8500 employees across 38 countries. The $2.9 billion company is listed on the Bombay Stock Exchange and the National Stock Exchange of India and is the flagship telecoms arm of the $103.3 billion Tata Group.
                    </Text>
                    <Text style={styles.para}>
                        The exponential growth of connected devices, emergence of social media, analytics, and cloud computing (SMAC), and acceptance of bring your own device (BYOD), are all resulting in a major transition in the way enterprises engage with technology. Both developed and emerging economies are looking to innovation in technology. As a key enabler of information and communication technologies to global enterprises, Tata Communications has led from the front in ensuring a robust digital ecosystem that is equipped for the future – with the infrastructure that can cope with customers’ demands of intelligence, scalability and flex.
                    </Text>
                    <Text style={styles.para}>
                        Our strategic portfolios are responsive to changing business needs.
                        Tata Communications’ enhanced business strategy has the consumer’s requirements, trends and movements at the heart of everything it does. It seeks to create an open infrastructure, partner ecosystem and platform that is fit for business and delivers that ‘just works’ experience, whilst retaining the transparency, flexibility and control that CEO/CIO’s require to safeguard and enhance their organisations’ customer experience and brand reputation.  All of this is overlaid onto years of investment and infrastructure through its $1.19 billion investment in the world’s only wholly owned subsea fibre network that circles the globe.
                    </Text>
                    <Text style={styles.para}>
                        Tata Communications’ services portfolio includes predictable high-speed connections and global MPLS virtual private networks, Telepresence services, DDoS mitigation and detection service, content delivery networks and cloud offerings. Tata Communications offers customised network solutions for customers in key markets – including verticals like manufacturing, oil and gas, banking, financial services and insurance, and media and entertainment – offering our customers speed, quality and unparalleled network reach.
                    </Text>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create(
    {
        container:
        {
            flex: 1,
            backgroundColor: 'white'
        },
        headerStyle:
        {
            flexDirection: 'row',
            height: '10%',
            backgroundColor: "#3c86c5",
            paddingLeft: 10,
            paddingTop: 25,
            shadowColor: "black",
            elevation: 2,
            borderBottomWidth: 0.25,
            borderBottomColor: 'grey',
            elevation: 1
        },
        headerText:
        {
            flex: 7,
            fontWeight: '700',
            fontSize: 20,
            color: 'white',
            alignSelf: 'center',
            paddingLeft: 5,
            paddingBottom: 8
        },
        para:
        {
            padding: 10
        }
    }
)