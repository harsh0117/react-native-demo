import { AppRegistry } from 'react-native';
import App from './App';

import { YellowBox } from 'react-native'

YellowBox.ignoreWarnings([
    'Warning: isMounted(...) is deprecated',
    'Module RCTImageLoader requires',
    'Method `jumpToIndex` is deprecated',
    'You should only render one navigator explicitly',
    'Warning: Failed child context type:',
    'Class RCTCxxModule was not exported.',
    'Warning: Failed prop type:',
    'Warning: Can\'t call setState'  
    ]);

AppRegistry.registerComponent('Weather', () => App);
